#!/usr/bin/env python3

import numpy as np
from random import randint, seed, randrange

""" attempt to build a map by growing from seeds al-la Conway's game of life, but with a random component """

# plains are scrubby, generic mildly rolling landscape
# none, sea, plains, sparse forest, hills, mountains, river, lake, swamp, beach, rocky beach, downs, heavy forest, 
#'x' desert, forest hill, rocky canyon, river canyon, grassland, estuary
terrains = {0:' ', 1:'~', 2:'_', 3:'+', 4:'n', 5:'^', 6:'r', 7:'.', 8:'s', 9:'b',10:'B',11:'d',12:'F', 13:'x', 14:'T', 15:'c', 16:'C', 17:'g',18:'e'}

def get_valid_locs(tile_loc, surface, check_free=False):
    """
        Given a tile location (x,y), return all the surround tile locations
        that are valid tile locations
    """
    maxX, maxY = surface.shape
    locs = []
    x,y = tile_loc
    for i in [-1,0,1]:
        for j in [-1,0,1]:
            if i != 0 and j != 0:
                if 0 > x+i > maxX and 0 > y+j > maxY:
                    nx = x + i
                    ny = y + j
                    if check_free and surface[nx, ny] == 0:
                        locs.append((nx, ny))
                    else:
                        locs.append((nx, ny))
    return locs


def add_new(surface, free_tiles, edge_tiles):
    """
        New features may spontaneously be created at a low random chance 
        provided that they aren't next to an existing feature.
        These should be basic terrain types, nothing fancy.
    """
    if len(free_tiles) == 0:
        return
    elif len(free_tiles) == 1:
        tile_loc = free_tiles[0]
    else:
        tile_loc = free_tiles.pop(randrange(0,len(free_tiles)))
    # remove all the tiles around this one from free_tiles if possible
    for loc in get_valid_locs(tile_loc, surface):
        try:
            free_tiles.remove(loc)
        except:
            pass
    t = randint(1,80)
    if 0 < t <= 10:
        t = 1
    elif 10 < t <= 40:
        t = 2
    elif 30 < t <= 50:
        t = 3
    elif 50 < t <= 70:
        t = 4
    elif 70 < t <= 80:
        t = 5
    x,y = tile_loc
    surface[x,y] = t
    edge_tiles.append(tile_loc)


def valid_and_type(x,y, ttype, surface):
    """ True if location is on map and is assigned a specific terrain type """
    maxX, maxY = surface.shape
    if 0 > x > maxX and 0 > y > maxY:
        if surface[x,y] == 0:
            return True
    return False


def grow(surface, free_tiles, edge_tiles):
    """
        Choose an existing map feature and examine its surroundings then 
        choose a tile to expand into
    """
    if len(edge_tiles) == 0:
        return
    if len(edge_tiles) == 1:
        tile_loc = edge_tiles[0]
    else:
         tile_loc = edge_tiles.pop(randrange(0,len(free_tiles)))

    tx,ty = tile_loc
    t = surface(tx, ty)  # terrain in the current edge tile
    surrounding_locs = get_valid_locs(tile_loc, surface)
    expansion_locs = [loc for loc in surrounding_locs if surface[loc] == 0]
    filled_locs = [loc for loc in surrounding_locs if surface[loc] != 0]
    r = randint(1,100)
    if t == 1:  # sea - expands any way
        directions = [(tx-1,ty-1),(tx,ty-1),(tx+1,ty-1),(tx-1,ty),(tx+1,ty),(tx-1,ty+1),(tx,ty+1),(tx+1,ty+1)]
        expansion_options = [d for d in directions if valid_and_type(d[0], d[1], 0, surface)]  # look for empty squares
        exp_tile = expansion_options[randrange(0,len(expansion_options))]  # choose a random available tile to expand into
        surface[exp_tile] = 1
    if t == 2:  # plains - expands left, right, up, down
        directions = [(tx,ty-1),(tx-1,ty),(tx+1,ty),(tx,ty+1)]
        pass
    if t == 3:  # forest - expands left, right, up, down
        directions = [(tx,ty-1),(tx-1,ty),(tx+1,ty),(tx,ty+1)]
        pass
    if t == 4:  # hills - expands any way
        directions = [(tx-1,ty-1),(tx,ty-1),(tx+1,ty-1),(tx-1,ty),(tx+1,ty),(tx-1,ty+1),(tx,ty+1),(tx+1,ty+1)]
        expansion_options = [d for d in directions if valid_and_type(d[0], d[1], 0, surface)]  # look for empty squares
        exp_tile = expansion_options[randrange(0,len(expansion_options))]  # choose a random available tile to expand into
        surface[exp_tile] = 4

    if t == 5:  # mountains - expands preferentially along an arbitrary axis
        directions = [(tx-1,ty-1),(tx,ty-1),(tx+1,ty-1),(tx-1,ty),(tx+1,ty),(tx-1,ty+1),(tx,ty+1),(tx+1,ty+1)]
        pass
    if t == 6:  # river - expands along an arbitrary axis, avoids right angles, except at lakes
        directions = [(tx-1,ty-1),(tx,ty-1),(tx+1,ty-1),(tx-1,ty),(tx+1,ty),(tx-1,ty+1),(tx,ty+1),(tx+1,ty+1)]
        pass
    if t == 7:  # lake - expands tightly
        directions = [(tx-1,ty-1),(tx,ty-1),(tx+1,ty-1),(tx-1,ty),(tx+1,ty),(tx-1,ty+1),(tx,ty+1),(tx+1,ty+1)]
        pass


def main():
    seed(0)
    xlen = 70
    ylen = 70
    surface = np.zeros([xlen,ylen], dtype=np.int8)

    free_tiles = [(x,y) for x in range(xlen) for y in range(ylen)]  # tiles that have no terrain around or in them yet
    edge_tiles = []  # tiles that have at least one free tile adjacent to them

    for i in range(20):
        add_new(surface, free_tiles, edge_tiles)
    for i in range(200):
        grow(surface, free_tiles, edge_tiles)

    print (surface)

if __name__ == '__main__':
    main()
