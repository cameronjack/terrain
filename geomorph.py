#!/usr/bin/env python3

import numpy as np

class feature(object):
    def __init__(self):
        state = None

class air_feature(feature):
    def __init__(self):
        state = 'air'

class liquid_feature(feature):
    def __init__(self):
        state = 'liquid'

class solid_feature(feature):
    def __init__(self):
        state = 'solid'  # sedimentary is also an option
        hardness = 0  # moh's scale
        density = 0  # g/cm^3
        strength = 0  # out of 10
        toughness = 0  # out of 10




class world(object):
    def __init__(self):
        grid = np.array([50,50,10], dtype=feature)

    def uplift(self):
        pass

    def evaporate(self):
        pass

    def rain(self):
        pass

    def water_erode(self):
        pass

    def wind_erode(self):
        pass
